<?php

namespace App\Http\Controllers;

use App\Http\Requests\ToDoCreateRequest;
use App\Http\Requests\ToDoUpdateRequest;
use App\Services\ToDoService;
use Inertia\Inertia;
use Inertia\Response;

class ToDoController extends Controller
{
    private ToDoService $toDoService;

    public function __construct(ToDoService $toDoService)
    {
        $this->toDoService = $toDoService;
    }

    public function index(): Response
    {
        return Inertia::render('ToDoPage', [
            'toDoList' => $this->toDoService->getToDoList(),
        ]);
    }

    public function store(ToDoCreateRequest $request): string
    {
        return $this->toDoService
            ->addToDo($request)
            ->toJson();
    }

    public function update(ToDoUpdateRequest $request, int $id): bool
    {
        return $this->toDoService->updateToDo($request, $id);
    }

    public function destroy(string $id): bool
    {
        return $this->toDoService->deleteToDo($id);
    }
}
