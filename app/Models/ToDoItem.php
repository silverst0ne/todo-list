<?php

namespace App\Models;

use Carbon\Carbon;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ToDoItem
 *
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $title
 * @property bool $completed
 * @method static Builder|ToDoItem newModelQuery()
 * @method static Builder|ToDoItem newQuery()
 * @method static Builder|ToDoItem query()
 * @method static Builder|ToDoItem whereCompleted($value)
 * @method static Builder|ToDoItem whereCreatedAt($value)
 * @method static Builder|ToDoItem whereId($value)
 * @method static Builder|ToDoItem whereTitle($value)
 * @method static Builder|ToDoItem whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ToDoItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'completed',
    ];

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getCreatedAt(): ?Carbon
    {
        return $this->created_at;
    }

    public function getUpdatedAt(): ?Carbon
    {
        return $this->updated_at;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function isCompleted(): bool
    {
        return $this->completed;
    }

    public function setCompleted(bool $completed): void
    {
        $this->completed = $completed;
    }
}
