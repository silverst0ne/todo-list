<?php

namespace App\Services;

use App\Http\Requests\ToDoCreateRequest;
use App\Http\Requests\ToDoUpdateRequest;
use App\Models\ToDoItem;
use App\Repositories\ToDoRepository;
use Illuminate\Database\Eloquent\Collection;

class ToDoService
{
    private ToDoRepository $toDoRepository;

    public function __construct(ToDoRepository $toDoRepository)
    {
        $this->toDoRepository = $toDoRepository;
    }

    public function getToDoList(): Collection
    {
        return $this->toDoRepository->all();
    }

    public function addToDo(ToDoCreateRequest $request): ToDoItem
    {
        return $this->toDoRepository->add($request);
    }

    public function updateToDo(ToDoUpdateRequest $request, int $id): bool
    {
        return $this->toDoRepository->update($request, $id);
    }

    public function deleteToDo(int $id): bool
    {
        return $this->toDoRepository->delete($id);
    }
}
