<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Inertia::share([
            'locale' => function () {
                return app()->getLocale();
            },
            'lang' => function () {
                $file = resource_path('locales/'. app()->getLocale() .'/'.app()->getLocale() .'.json');
                return file_exists($file)
                    ? json_decode(file_get_contents($file), true)
                    : [];
            },
        ]);
    }
}
