<?php

namespace app\Repositories\Contracts;

use App\Http\Requests\ToDoCreateRequest;
use App\Http\Requests\ToDoUpdateRequest;
use App\Models\ToDoItem;
use Illuminate\Database\Eloquent\Collection;

interface ToDoRepositoryInterface
{
    public function all(): Collection;
    public function add(ToDoCreateRequest $request): ToDoItem;
    public function update(ToDoUpdateRequest $request, int $id): bool;
    public function delete(int $id): bool;
}
