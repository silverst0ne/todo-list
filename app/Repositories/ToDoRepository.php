<?php

namespace App\Repositories;

use App\Http\Requests\ToDoCreateRequest;
use App\Http\Requests\ToDoUpdateRequest;
use App\Models\ToDoItem;
use App\Repositories\Contracts\ToDoRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ToDoRepository implements ToDoRepositoryInterface
{

    public function all(): Collection
    {
        return ToDoItem::query()
            ->orderBy('id')
            ->get();
    }

    public function add(ToDoCreateRequest $request): ToDoItem
    {
        return ToDoItem::create($request->validated());
    }

    public function update(ToDoUpdateRequest $request, int $id): bool
    {
        $toDoItem = ToDoItem::findOrFail($id);
        $toDoItem->fill($request->validated());

        return $toDoItem->save();
    }

    public function delete(int $id): bool
    {
        return ToDoItem::destroy($id) > 0;
    }
}
